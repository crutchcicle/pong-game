#include "..\headers\game_logic.h"

int padSpeed = 5;
int ballSpeed = 2;
float pad1y = 0.0;
float pad2y = 0.0;
float padWidth = 0.015;
float padHeight = 0.15;
float step = 0.01;
signed char pad1dir = 0;
signed char pad2dir = 0;
float ballX = 0.5;
float ballY = 0.5;
float ballR = 0.015;
float ballA = M_PI * 7 / 8;

void KeyboardPress(unsigned char code, int x, int y) {
	switch (code) {
		case 27 :
			exit(0);
		break;
		case 'W' : case 'w' :
			pad1dir = -1;
		break;
		case 'S' : case 's' :
			pad1dir = 1;
		break;
		case 'I' : case 'i' :
			pad2dir = -1;
		break;
		case 'K' : case 'k' :
		  pad2dir = 1;
	}
}

void KeyboardRelease(unsigned char code, int x, int y) {
	switch (code) {
		case 'W' : case 'w' :
		case 'S' : case 's' :
			pad1dir = 0;
		break;
		case 'I' : case 'i' :
		case 'K' : case 'k' :
			pad2dir = 0;
	}
}

void SKeyboard(int code, int x, int y) {
	
}

void PadMovement(int id) {
	char change = 0;
	if (pad1dir == -1)
		if (pad1y - step >= 0) {
			pad1y = pad1y - step;
			change = 1;
		}
	if (pad1dir == 1)
		if (pad1y + padHeight + step < 1) {
			pad1y = pad1y + step;
			change = 1;
		}
	if (pad2dir == -1)
		if (pad2y - step >= 0) {
			pad2y = pad2y - step;
			change = 1;
		}
	if (pad2dir == 1)
		if (pad2y + padHeight + step < 1) {
			pad2y = pad2y + step;
			change = 1;
		}
	glutTimerFunc(padSpeed, PadMovement, 0);
	if (change)
		glutPostRedisplay();
}

void BallMovement(int id) {
	if (ballY - ballR < 0)
		ballA = M_PI * 2 - ballA;
	if (ballY + ballR > 1)
		ballA = M_PI * 2 - ballA;
	if (ballX - ballR < 0) {
		ballX = 0.5;
		ballY = 0.5;
	}
	if (ballX + ballR > 1) {
		ballX = 0.5;
		ballY = 0.5;
	}
	if (ballX - ballR < padWidth)
		if (ballY >= pad1y && ballY <= pad1y + padHeight)
			ballA = M_PI - ballA;
	if (ballX + ballR > 1 - padWidth)
		if (ballY >= pad2y && ballY <= pad2y + padHeight)
			ballA = M_PI - ballA;
	ballX += cos(ballA) * 0.002;
	ballY += sin(ballA) * 0.002;
	glutTimerFunc(ballSpeed, BallMovement, 0);
	glutPostRedisplay();
}