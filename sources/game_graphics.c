#include "..\headers\game_graphics.h"

extern float pad1y;
extern float pad2y;
extern float padWidth;
extern float padHeight;
extern float ballX;
extern float ballY;
extern float ballR;

void glCircle4f(float x, float y, float z, float r) {
	glBegin(GL_POLYGON);
	float angle = 0.0;
	for (int i = 0; i < 360; i++) {
		glVertex3f(x + cos(angle) * r / 4 * 3 , y + sin(angle) * r, z);
		angle += 2 * M_PI / 360;
	}
	glEnd();
}

void Reshape(int w, int h) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	glOrtho(0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
}

void Draw(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glColor3f(1.0f, 1.0f, 1.0f);
	glSquare4f( 0.0,
							pad1y,
							padWidth,
							pad1y + padHeight);
	glSquare4f( 1 - padWidth,
							pad2y,
							1,
							pad2y + padHeight);
	glCircle4f(ballX, ballY, 0, ballR);
	glutSwapBuffers();
}