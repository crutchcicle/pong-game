#include <GL\gl.h>
#include <GL\glut.h>
#include <stdlib.h>
#include <stdio.h>

#include "..\headers\game_logic.h"
#include "..\headers\game_graphics.h"

extern int padSpeed;
extern int ballSpeed;
extern float padWidth;
extern float padHeight;
extern float ballX;
extern float ballY;
extern float ballR;

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutGameModeString("2000x2000:32");
	glutEnterGameMode();
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);
	glutIgnoreKeyRepeat(0);
	glutSpecialFunc(SKeyboard);
	glutKeyboardFunc(KeyboardPress);
	glutKeyboardUpFunc(KeyboardRelease);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glutTimerFunc(padSpeed, PadMovement, 0);
	glutTimerFunc(ballSpeed, BallMovement, 0);
	glutMainLoop();
	return 0;
}