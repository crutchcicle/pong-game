#ifndef GAME_LOGIC_H_INCLUDED
#define GAME_LOGIC_H_INCLUDED

#include <GL\gl.h>
#include <GL\glut.h>
#include <math.h>
#include <stdlib.h>

void KeyboardPress(unsigned char, int, int);
void KeyboardRelease(unsigned char, int, int);
void SKeyboard(int, int, int);
void PadMovement(int);
void BallMovement(int);

#endif