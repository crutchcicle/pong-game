#ifndef GAME_GRAPHICS_H_INCLUDED
#define GAME_GRAPHICS_H_INCLUDED

#include <GL\gl.h>
#include <GL\glut.h>
#include <math.h>

#define glSquare4f(topLeftX, topLeftY, bottomRightX, bottomRightY)\
{glBegin(GL_QUADS);\
glVertex3f(topLeftX, topLeftY, 0);\
glVertex3f(bottomRightX, topLeftY, 0);\
glVertex3f(bottomRightX, bottomRightY, 0);\
glVertex3f(topLeftX, bottomRightY, 0);\
glEnd();}

void glCircle4f(float, float, float, float);
void Reshape(int, int);
void Draw(void);

#endif