COM   = gcc
FLAGS =
LIBS  = -lopengl32 -lglu32 -L. -lglut32
EXEC  = Pong.exe
OBJS  = $(patsubst sources/%.c,objects/%.o,$(wildcard sources/*.c))

all: debug

.PHONY: cleanobj cleanexec cleanall debug release build cleanzip zip

cleanall: cleanobj cleanexec
	
cleanobj:
	del /q objects\*

cleanexec:
	del $(EXEC)
	
debug: FLAGS = -Wall
debug: cleanall $(OBJS) build

release: FLAGS = -flto -O3 -w
release: cleanall $(OBJS) build

build:
	$(COM) $(FLAGS) $(OBJS) $(LIBS) -o $(EXEC)

$(OBJS): objects/%.o : sources/%.c
	$(COM) $(FLAGS) -m32 -c -std=gnu99 $< -o $@

cleanzip:
	del $(EXEC)
	
zip: cleanzip
	zip -r "Pong.zip" $(EXEC) glut32.dll